<?php

namespace Drupal\entityqueue_blocks\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\entityqueue\Entity\EntityQueue;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver class for providing blocks for each entity queue.
 */
class EntityQueueBlock extends DeriverBase implements ContainerDeriverInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $queues = EntityQueue::loadMultipleByTargetType('node');

    foreach ($queues as $queue) {
      $this->derivatives[$queue->id()] = $base_plugin_definition;
      $this->derivatives[$queue->id()]['admin_label'] = t('Queue: @label', ['@label' => $queue->label()]);
    }

    return $this->derivatives;
  }

}
