<?php

namespace Drupal\entityqueue_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entityqueue\Entity\EntitySubqueue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'NodeBlock' block plugin.
 *
 * @Block(
 *   id = "entityqueue_block",
 *   admin_label = @Translation("EntityQueue block"),
 *   deriver = "Drupal\entityqueue_blocks\Plugin\Derivative\EntityQueueBlock"
 * )
 */
class EntityQueueBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * View builder.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $viewBuilder;

  /**
   * Node storage manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $nodeStorage;

  /**
   * Entity queue we are building a block for.
   *
   * @var Drupal\entityqueue\Entity\EntitySubqueue
   */
  private $queue;

  /**
   * Creates a NodeBlock instance.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Block plugin ID.
   * @param mixed $plugin_definition
   *   Definition of the plugin implementation.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->viewBuilder = $entity_manager->getViewBuilder('node');
    $this->nodeStorage = $entity_manager->getStorage('node');
    $this->queue = EntitySubqueue::load($this->getDerivativeId());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('entity_type.manager')
        );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = $this->queue->get('items')?->getValue();
    $display_mode = $this->configuration['display_mode'];
    $wrapper_class = $this->configuration['wrapper_class'];

    $build = [];
    foreach ($items as $item) {
      $nid = $item['target_id'];
      $node = $this->nodeStorage->load($nid);

      $build[] = [
        '#type' => 'container',
        '#attributes' => ['class' => [$wrapper_class]],
        'content' => $this->viewBuilder->view($node, $display_mode),
      ];

    }

    $build['#cache']['tags'][] = 'config:block_config:' . $wrapper_class;
    $build['#cache']['tags'][] = 'config:block_config:' . $display_mode;
    $build['#cache']['tags'][] = 'entity_subqueue:' . $this->queue->id();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $settings = parent::defaultConfiguration();

    $settings['display_mode'] = 'vertical_teaser';
    $settings['wrapper_class'] = 'campl_column12';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['wrapper_class'] = [
      '#type' => 'select',
      '#title' => 'Number of columns',
      '#options' => [
        'campl-column12' => '1 column (full width)',
        'campl-column6' => '2 columns',
        'campl-column4' => '3 columns',
        'campl-column3' => '4 columns',
      ],
      '#default_value' => $this->configuration['wrapper_class'],
    ];

    $form['display_mode'] = [
      '#type' => 'select',
      '#title' => 'Display mode',
      '#options' => [
        'teaser' => 'Horizontal',
        'vertical_teaser' => 'Vertical',
        'focus_on_teaser' => 'Focus on Teaser',
        'news_listing_item' => 'News listing',
      ],
      '#description' => '<em>Horizontal</em> will display the image to the left, and then any text to the right. <em>Vertical</em> will display the image above the text',
      '#default_value' => $this->configuration['display_mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display_mode'] = $form_state->getValue('display_mode');
    $this->configuration['wrapper_class'] = $form_state->getValue('wrapper_class');
  }

}
