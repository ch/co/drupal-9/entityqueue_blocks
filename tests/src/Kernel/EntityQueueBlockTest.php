<?php

namespace Drupal\Tests\entityqueue_blocks\Kernel;

use Drupal\entityqueue\Entity\EntityQueue;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test blocks provided by entityqueue_blocks.
 *
 * @group entityqueue_blocks
 */
class EntityQueueBlockTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['user', 'node', 'entityqueue', 'entityqueue_blocks'];

  /**
   * Theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Name of test queue.
   *
   * @var string
   */
  protected $testQueue;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_subqueue');

    $this->testQueue = $this->randomMachineName();

    // Create a test queue.
    $entity_queue = EntityQueue::create([
      'id' => $this->testQueue,
      'label' => $this->randomString(),
      'handler' => 'simple',
      'entity_settings' => [
        'target_type' => 'node',
      ],
    ]);

    $entity_queue->save();

  }

  /**
   * Test block which we expect for test queue.
   */
  public function testEntityQueueBlockSettings() {
    $definitions = $this->container->get('plugin.manager.block')->getDefinitions();
    $block_ids = array_keys($definitions);

    $expected_id = 'entityqueue_block:' . $this->testQueue;

    $this->assertTrue(in_array($expected_id, $block_ids), "Block definition for $expected_id found");
  }

}
